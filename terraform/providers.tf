provider "cloudflare" {
  version = "2.19.2"
  email   = "cmcdougle@gmail.com"
  api_key = var.api_key
}
