variable "domain-name" {
  description = "domain name"
  type        = string
}

variable "root-bucket-name" {
  description = "root bucket name"
  type        = string
}

variable "www-bucket-name" {
  description = "www bucket name"
  type        = string
}

variable "dev-bucket-name" {
  description = "dev bucket name"
  type        = string
}

variable "mx0" {
  description = "zeroeth mx record"
  type        = string
}

variable "mx1" {
  description = "first mx record"
  type        = string
}

variable "mx2" {
  description = "second mx record"
  type        = string
}

variable "mx3" {
  description = "third mx record"
  type        = string
}

variable "mx4" {
  description = "fourth mx record"
  type        = string
}
variable "api_key" {
  description = "api key for cloudflare"
  type        = string
}

