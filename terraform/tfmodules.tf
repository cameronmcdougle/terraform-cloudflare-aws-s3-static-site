

module "root-s3-bucket" {
  source = "./modules/aws/root"

  root-bucket-name = var.root-bucket-name
  tags = {
    Terraform   = "true"
    Environment = "prod"
  }
}

module "www-s3-bucket" {
  source = "./modules/aws/www"

  www-bucket-name = var.www-bucket-name

  tags = {
    Terraform   = "true"
    Environment = "prod"
  }

}

module "dev-s3-bucket" {
  source = "./modules/aws/dev"

  dev-bucket-name = var.dev-bucket-name

  tags = {
    Terraform   = "true"
    Environment = "prod"
  }

}



