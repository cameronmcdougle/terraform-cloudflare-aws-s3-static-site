resource "aws_s3_bucket" "root-s3-bucket" {
  bucket = var.root-bucket-name
  acl    = "public-read"

  website {
    index_document = "index.html"
    error_document = "error.html"

  }

  tags = var.tags
}

data "aws_iam_policy_document" "public-read" {
  statement {
    sid    = "PublicReadGetObject"
    effect = "Allow"
    actions = [
      "s3:GetObject"
    ]

    principals {
      type        = "*"
      identifiers = ["*"]
    }

    resources = [
      "arn:aws:s3:::${var.root-bucket-name}/*"
    ]
  }
}

resource "aws_s3_bucket_policy" "public-read" {
  bucket = aws_s3_bucket.root-s3-bucket.bucket
  policy = data.aws_iam_policy_document.public-read.json
}

