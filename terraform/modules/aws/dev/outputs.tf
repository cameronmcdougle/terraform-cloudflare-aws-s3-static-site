output "arn" {
  description = "ARN of the bucket"
  value       = aws_s3_bucket.dev-s3-bucket.arn
}

output "name" {
  description = "Name (id) of the bucket"
  value       = aws_s3_bucket.dev-s3-bucket.id
}

output "domain" {
  description = "Domain name of the bucket"
  value       = aws_s3_bucket.dev-s3-bucket.website_domain
}

output "website_endpoint" {
  description = "DNS endpoint of bucket"
  value       = aws_s3_bucket.dev-s3-bucket.website_endpoint
}

