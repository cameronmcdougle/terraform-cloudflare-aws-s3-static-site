resource "aws_s3_bucket" "www-s3-bucket" {
  bucket = var.www-bucket-name
  acl    = "public-read"

  website {
    redirect_all_requests_to = "http://mcdougz.com"
  }

  tags = var.tags
}

data "aws_iam_policy_document" "public-read" {
  statement {
    sid    = "PublicReadGetObject"
    effect = "Allow"
    actions = [
      "s3:GetObject"
    ]

    principals {
      type        = "*"
      identifiers = ["*"]
    }

    resources = [
      "arn:aws:s3:::${var.www-bucket-name}/*"
    ]
  }
}

resource "aws_s3_bucket_policy" "public-read" {
  bucket = aws_s3_bucket.www-s3-bucket.bucket
  policy = data.aws_iam_policy_document.public-read.json
}

