resource "cloudflare_record" "txt-google-apps" {
  zone_id = cloudflare_zone.zone.id
  name    = "@"
  value   = "INSERT-TXT"
  type    = "TXT"
}

resource "cloudflare_record" "cname-root" {
  zone_id = cloudflare_zone.zone.id
  name    = var.domain-name
  value   = module.root-s3-bucket.website_endpoint
  type    = "CNAME"
}

resource "cloudflare_record" "cname-www" {
  zone_id = cloudflare_zone.zone.id
  name    = "www"
  value   = module.www-s3-bucket.website_endpoint
  type    = "CNAME"
}

resource "cloudflare_record" "cname-dev" {
  zone_id = cloudflare_zone.zone.id
  name    = "dev"
  value   = module.dev-s3-bucket.website_endpoint
  type    = "CNAME"
}
