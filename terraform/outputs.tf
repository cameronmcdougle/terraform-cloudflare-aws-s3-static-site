output "cf-ns" {
  description = "nameservers"
  value       = cloudflare_zone.zone.name_servers
}

output "cf-zone-id" {
  description = "zone id"
  value       = cloudflare_zone.zone.id
}
